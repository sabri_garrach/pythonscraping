#!/usr/bin/env python
# coding: utf-8

#pip3 install requests
#pip3 install bs4

from bs4 import BeautifulSoup

import requests
#from selenium import webdriver
from csv import writer
import csv,json


     ## scrap data from https://bskimmobilier.com
with open('housing.csv', 'a', encoding='utf8', newline='\n') as f:
    thewriter = writer(f)
    header = ['bienPieces', 'bienType', 'transactionType', 'description','prix','images','originUrl','address']
    thewriter.writerow(header)
for x in range(1,5):
 wiki=requests.get('https://bskimmobilier.com/acheter-louer?page='+str(x)+'&transaction=buy')
 soup=BeautifulSoup(wiki.text,'html')
 print(soup.find('title'))
# ### trouver html tags with classes
 result=[]
 house_contents=soup.find_all("div",class_='list-item')

 print("Biens de vents :")
 for house in house_contents:

    price_element = house.find("span", class_ ="price pull-right")
    adress_element = house.find("div", class_ ="adresse dpt-77 text-uppercase text-muted")
    type_element = house.find("div", class_ ="announce-title text-uppercase")
    piece_element = house.find("span", class_ ="room")
    description_element = house.find("div", class_ ="announce text-justify hidden-md")
    image_element = house.find("div", class_ ="img left")
    reference_element = house.find("div", class_ ="details")
    fdescription_element = description_element.text.replace("\n"," ")
    freference_element = reference_element.div;
    fOriginUrl = house.a.attrs['href']
    
    sreference_element=''
    if not (reference_element) :
        freference_element='000'
    else :  freference_element = reference_element
    for bien in reference_element :
         freference_element=bien.text
    for char in freference_element :
        if(char.isdecimal()):
           sreference_element=sreference_element+char
    fdescription_element.replace("❗️"," ")
    
    infoData = [sreference_element,type_element.text,'Vente', fdescription_element.strip().replace(","," "),price_element.text[0:7], 'https://bskimmobilier.com/'+image_element.img.attrs['data-original'],'https://bskimmobilier.com'+fOriginUrl,'']
    result.append(infoData)
 with open('housing.csv', 'a', encoding='utf8', newline='\n') as f:
    thewriter = writer(f)
    for item in result:
        thewriter.writerow(item)

for x in range(0,5):
 wiki=requests.get('https://bskimmobilier.com/acheter-louer?page='+str(x)+'&transaction=rent')
 soup=BeautifulSoup(wiki.text,'html')
 print(soup.find('title'))
 print (wiki, 'page =' ,x)
 result=[]
 house_contents=soup.find_all("div",class_='list-item')

 print("Biens de location :")
 for house in house_contents:

    price_element = house.find("span", class_ ="price pull-right")
    adress_element = house.find("div", class_ ="adresse dpt-77 text-uppercase text-muted")
    type_element = house.find("div", class_ ="announce-title text-uppercase")
    piece_element = house.find("span", class_ ="room")
    description_element = house.find("div", class_ ="announce text-justify hidden-md")
    image_element = house.find("div", class_ ="img left")
    reference_element = house.find("div", class_ ="details")
    fdescription_element = description_element.text.replace("\n"," ")
    freference_element = reference_element.div;
    fOriginUrl = house.a.attrs['href']
    print(fOriginUrl)

    fdescription_element = description_element.text.replace("\n"," ")
    freference_element = reference_element.div;
    fOriginUrl = house.a.attrs['href']
    
    sreference_element=''
    if not (reference_element) :
        freference_element='000'
    else :  freference_element = reference_element
    for bien in reference_element :
         freference_element=bien.text
    for char in freference_element :
        if(char.isdecimal()):
           sreference_element=sreference_element+char
    fdescription_element.replace("❗️"," ")
    images=[]
    images.append('https://bskimmobilier.com'+image_element.img.attrs['data-original'])
    infoData = [int(sreference_element),type_element.text,'location', fdescription_element.strip().replace(","," "),int(price_element.text[0:3].replace(" ","")),'https://bskimmobilier.com'+image_element.img.attrs['data-original'] ,'https://bskimmobilier.com'+fOriginUrl,'']
    result.append(infoData)
 with open('housing.csv', 'a', encoding='utf8', newline='\n') as f:
      thewriter = writer(f)
      for item in result:
       thewriter.writerow(item)
 
#objet pour la conversion csv to Json
data = []
with open('housing.csv',encoding='utf8') as fcsv :
  csvReader = csv.DictReader(fcsv) 
  for rows in csvReader : 
       data.append(rows)
  for row in data :
     print ('bienPieces :',row['bienPieces'],'\n')
     row['bienPieces']=int(row['bienPieces'])
     row['prix']=int(row['prix'].replace(" ",""))
     row['images']=row['images'].split()
     if (row['bienType'].__contains__('Appartement')):
        row['bienType']='appartement'
     elif (row['bienType'].__contains__('Maison')):
          row['bienType']='maison'
     if (row['bienType'].__contains__('Terrain')):
        row['bienType']='terrain'
     elif (row['bienType'].__contains__('Local D')or row['bienType'].__contains__('Local C')or row['bienType'].__contains__('Locaux')):
          row['bienType']='localcommercial'
     if (row['bienType'].__contains__('Immeuble')):
        row['bienType']='immeuble'
     elif (row['bienType'].__contains__('Boutique')):
          row['bienType']='boutique'
     if (row['bienType'].__contains__('Parking')):
        row['bienType']='parking'
     elif (row['bienType'].__contains__('Bureau')):
          row['bienType']='bureau'
     if (row['bienType'].__contains__('Ferme')):
        row['bienType']='ferme'
     elif (row['bienType'].__contains__('Chateau')):
          row['bienType']='chateau'
     if (row['bienType'].__contains__('Loft')):
        row['bienType']='loft'
     elif (row['bienType'].__contains__('chambre de hot')):
          row['bienType']='chambredhots'
with open('housing.json','a',encoding='utf-8') as jsonFile :
    jsonFile.write(json.dumps(data,indent=4))

