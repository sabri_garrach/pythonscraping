from bs4 import BeautifulSoup

import requests
#from selenium import webdriver
from csv import writer
import json,csv


    
with open('agenceSoleilLocation.csv', 'a', encoding='utf8', newline='\n') as f:
    thewriter = writer(f)
    header = ['bienPieces', 'bienType', 'transactionType', 'description','prix','images','originUrl','address']
    thewriter.writerow(header)

with open('agenceSoleilVente.csv', 'a', encoding='utf8', newline='\n') as f:
    thewriter = writer(f)
    header = ['bienPieces', 'bienType', 'transactionType', 'description','prix','images','originUrl','address']
    thewriter.writerow(header)
     ## scrap data from https://www.agencedusoleil.com/properties/page

for x in range(2):
 immobilier=requests.get('https://www.agencedusoleil.com/properties/page/'+str(x)+'/?filter-contract=SALE&filter-property-type&filter-price-from&filter-price-to&filter-rooms&filter-beds&filter-lot-area-from&filter-lot-area-to&filter-id')
 soup=BeautifulSoup(immobilier.text,'html.parser')
 print('Achat')
 print(soup.find('title').text)
# ### find html tags with classes
 result=[]
 urls=[]
 images=[]
 counter=0
 house_contents=soup.find_all("div",class_='property-row-content')
 url_content = soup.find_all("a", class_ ="property-row-image")
 for house in house_contents :

    localisation_element = house.find("div",class_='property-row-location')
    Pieces_element = house.find("span", class_ ="property-row-meta-item property-row-meta-item-type")
    description_element = house.find("div", class_ ="property-row-body")
    Prix_element = house.find("span", class_ ="property-row-meta-item property-row-meta-item-price")
    
    fPrix_element=Prix_element
    if not (Prix_element) :
        fPrix_element='888'
    else :  fPrix_element = Prix_element.strong.text[0:7]

    print('Localisation :',localisation_element.a.text)
    print('Type Bien:',Pieces_element.strong.text)
    print('Description :',description_element.text.strip()) 
    print('Prix :', fPrix_element.replace(" ","")) 
    print('\n')
    for urlOrigin in url_content :
        images.append(urlOrigin.img.attrs['src'])
        urls.append(urlOrigin.attrs['href'])
    counter+=1
    print (counter)
    infoData = [0,Pieces_element.strong.text,'vente',description_element.text.strip().replace(","," "),fPrix_element.replace("\xa0",""),images[counter] ,urls[counter],localisation_element.a.text]
    result.append(infoData)
with open('agenceSoleilVente.csv', 'a', encoding='utf8', newline='\n') as file :
 thewriter = writer(file)
 #thewriter.writerow(header)
 for item in result:
       thewriter.writerow(item)

for x in range(1):

 urls=[]
 images=[]
 counter=0
 print('Location')
 immobilier=requests.get('https://www.agencedusoleil.com/properties/page/'+str(x)+'/?filter-contract=RENT&filter-property-type&filter-price-from&filter-price-to&filter-rooms&filter-beds&filter-lot-area-from&filter-lot-area-to&filter-id')
 soup=BeautifulSoup(immobilier.text,'html')
 print(soup.find('title').text)
# ### find html tags with classes
 result=[]
 house_contents=soup.find_all("div",class_='property-row-content')
 url_content = soup.find_all("a", class_ ="property-row-image")
 for house in house_contents:

    localisation_element = house.find("div",attrs={'class':'property-row-location'})
    Pieces_element = house.find("span", class_ ="property-row-meta-item property-row-meta-item-type")
    description_element = house.find("div", class_ ="property-row-body")
    Prix_element = house.find("div", class_ ="property-row-meta")
    
    fPrix_element = Prix_element.span
    price = ""
 
    print('Type Bien:',Pieces_element.strong.text)
    print('Description :',description_element.text.strip()) 
    if not (Prix_element) :
        fPrix_element='999'
    else :  fPrix_element = Prix_element.span.strong.text
    for char in fPrix_element :
        if(char.isdecimal()):
            price=price+char
    print('Prix :',price)

    print('\n')
    for urlOrigin in url_content :
        images.append(urlOrigin.img.attrs['src'])
        urls.append(urlOrigin.attrs['href'])
    counter+=1
    print (counter)
    infoData = [0,Pieces_element.strong.text,'location',description_element.text.strip().replace(","," "),int(price),images[counter] ,urls[counter],localisation_element.a.text]
    result.append(infoData)

 with open('agenceSoleilLocation.csv', 'a', encoding='utf8', newline='\n') as file :
    thewriter = writer(file)
    #thewriter.writerow(header)
    for item in result:
     thewriter.writerow(item)

#objet pour la conversion csv to Json
data = []
with open('agenceSoleilLocation.csv',encoding='utf8') as fcsv :
  csvReader = csv.DictReader(fcsv) 
  for rows in csvReader : 
       data.append(rows)
  for row in data :
     print ('bienPieces :',row['bienPieces'],'\n')
     row['bienPieces']=int(row['bienPieces'])
     row['prix']=int(row['prix'].replace(" ",""))
     row['images']=row['images'].split()

     if (row['bienType'].__contains__('Appartement')):
        row['bienType']='appartement'
     elif (row['bienType'].__contains__('Maison')):
          row['bienType']='maison'
     if (row['bienType'].__contains__('Terrain')):
        row['bienType']='terrain'
     elif (row['bienType'].__contains__('Local D')or row['bienType'].__contains__('Local C')or row['bienType'].__contains__('Locaux')):
          row['bienType']='localcommercial'
     if (row['bienType'].__contains__('Immeuble')):
        row['bienType']='immeuble'
     elif (row['bienType'].__contains__('Boutique')):
          row['bienType']='boutique'
     if (row['bienType'].__contains__('Parking')):
        row['bienType']='parking'
     elif (row['bienType'].__contains__('Bureau')):
          row['bienType']='bureau'
     if (row['bienType'].__contains__('Ferme')):
        row['bienType']='ferme'
     elif (row['bienType'].__contains__('Chateau')):
          row['bienType']='chateau'
     if (row['bienType'].__contains__('Loft')):
        row['bienType']='loft'
     elif (row['bienType'].__contains__('chambre de hot')):
          row['bienType']='chambredhots'
with open('agence-soleil-location.json','w',encoding='utf-8') as jsonFile :
    jsonFile.write(json.dumps(data,indent=4))

#objet pour la conversion csv to Json
data = []
with open('agenceSoleilVente.csv',encoding='utf8') as fcsv :
  csvReader = csv.DictReader(fcsv) 
  for rows in csvReader : 
       data.append(rows)
  for row in data :
     print ('bienPieces :',row['bienPieces'],'\n')
     row['bienPieces']=int(row['bienPieces'])
     row['prix']=int(row['prix'])
     row['images']=row['images'].split()
     if (row['bienType'].__contains__('Appartement')):
        row['bienType']='appartement'
     elif (row['bienType'].__contains__('Maison')):
          row['bienType']='maison'
     if (row['bienType'].__contains__('Terrain')):
        row['bienType']='terrain'
     elif (row['bienType'].__contains__('Local D')or row['bienType'].__contains__('Local C')or row['bienType'].__contains__('Locaux')):
          row['bienType']='localcommercial'
     if (row['bienType'].__contains__('Immeuble')):
        row['bienType']='immeuble'
     elif (row['bienType'].__contains__('Boutique')):
          row['bienType']='boutique'
     if (row['bienType'].__contains__('Parking')):
        row['bienType']='parking'
     elif (row['bienType'].__contains__('Bureau')):
          row['bienType']='bureau'
     if (row['bienType'].__contains__('Ferme')):
        row['bienType']='ferme'
     elif (row['bienType'].__contains__('Chateau')):
          row['bienType']='chateau'
     if (row['bienType'].__contains__('Loft')):
        row['bienType']='loft'
     elif (row['bienType'].__contains__('chambre de hot')):
          row['bienType']='chambredhots'
with open('agence-soleil-vente.json','w',encoding='utf-8') as jsonFile :
    jsonFile.write(json.dumps(data,indent=4))

