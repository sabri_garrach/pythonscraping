#!/usr/bin/env python
# coding: utf-8

#pip3 install requests
#pip3 install bs4

from bs4 import BeautifulSoup

import requests
#from selenium import webdriver
from csv import writer
import csv,json

     ## scrap data from https://domians-immobilier.fr/recherche-avancee/page/
with open('domians-immo.csv', 'a', encoding='utf8', newline='\n') as file :
     header = ['bienPieces', 'bienType','transactionType', 'description','prix', 'images','originUrl','address']
     thewriter = writer(file)
     thewriter.writerow(header)
for x in range(1,3):
 immobilier=requests.get('https://domians-immobilier.fr/recherche-avancee/page/'+str(x)+'/?filter_search_action%5B0%5D=location&adv6_search_tab=location&term_id=97&adv_location&is2=1&submit=Rechercher')
 soup=BeautifulSoup(immobilier.text,'html')
 print(soup.find('title').text)
# ### trouver html tags with classes
 result=[]
 house_contents=soup.find_all("div",class_='property_listing')

 for house in house_contents:

    description_element = house.find("div", class_ ="listing_details the_list_view")
    Pieces_element = house.find("div", class_ ="property_listing_details")
    Nature_element = house.find("div", class_ ="property_agent_wrapper")
    Prix_element = house.find("div", class_ ="listing_unit_price_wrapper")
    image_element = house.find("div", class_ ="item active")
    Bien_element = house.find("div",class_='status-wrapper')
    fBien_element=Bien_element.div
    adresse_element = house.find("div", class_="property_location_image")
    Url_element = house.find("div",class_='listing_details the_grid_view')
    fUrl_element = Url_element.a.attrs['href']
    print(Url_element.a.attrs['href'])
 
    print('Piéces :',Pieces_element.find("span",attrs={'class':'infosize'}).text,'Piéces')
    print('Type annonce:',Nature_element.a.text)
    fPiece=int(Pieces_element.find("span",attrs={'class':'infosize'}).text[0:2])
    for bien in Bien_element : 
        fBien_element=bien.text
    print('Type Bien :',fPiece)
    print('Description :',description_element.text.strip())
    print('Prix :',Prix_element.text)
    print('image :',image_element.a.img.attrs['data-original'])
    print('Adresse ',adresse_element.a.text)
    print('\n')
    infoData = [int(str(Pieces_element.span.text[0:2])),fBien_element,Nature_element.a.text.lower(),description_element.text.strip().replace(",",""),Prix_element.text[0:3],image_element.a.img.attrs['data-original'],Url_element.a.attrs['href'],adresse_element.a.text]
    result.append(infoData)

 with open('domians-immo.csv', 'a', encoding='utf8', newline='\n') as file :
    header = ['bienPieces', 'bienType','transactionType', 'Description','Prix', 'Image','originUrl']
    thewriter = writer(file)
    #thewriter.writerow(header)
    for item in result:
        thewriter.writerow(item)


#scrapping la page vente sur site https://domians-immobilier.fr/recherche-avancee/page/
for x in range(1,4):
 immobilier=requests.get('https://domians-immobilier.fr/recherche-avancee/page/'+str(x)+'/?filter_search_action%5B0%5D=vente&adv6_search_tab=location&term_id=97&adv_location&is2=1&submit=Rechercher')
 soup=BeautifulSoup(immobilier.text,'html')
 print(soup.find('title').text)
# ### trouver html tags with classes
 result=[]
 house_contents=soup.find_all("div",class_='property_listing')

 for house in house_contents:

    description_element = house.find("div", class_ ="listing_details the_list_view")
    Pieces_element = house.find("span", class_ ="inforoom")
    Nature_element = house.find("div", class_ ="property_agent_wrapper")
    Prix_element = house.find("div", class_="listing_unit_price_wrapper")
    image_element = house.find("div", class_="item active")
    Bien_element = house.find("div",class_='status-wrapper')
    Url_element = house.find("div",class_='listing_details the_grid_view')
    fUrl_element = Url_element.a.attrs['href']
    adresse_element = house.find("div", class_="property_location_image")


    fdescription_element = description_element.text.strip().replace(","," ")
    fBien_element = Bien_element.div.text
    fNature_element = Bien_element.div.text
    fPrix_element = Prix_element.text
    fimage_element = image_element.a.img.attrs['data-original']
    fPieces_element = Pieces_element
    price = ""
    if not (Pieces_element) :
        fPieces_element='888'
    else :  fPieces_element = Pieces_element.text
    for char in fPrix_element :
        if(char.isdecimal()):
           price=price+char
    print('Prix :',price)

    print('Piéces :',fPieces_element)

    print('Type annonce:',fNature_element)
    for bien in Bien_element : 
     fBien_element=bien.text

    print('Type Bien :',fBien_element)

    print('Description :',fdescription_element)
    print('image :',fimage_element)
    print('\n')
    
    infoData = [int(fPieces_element),fBien_element,fNature_element.lower(),fdescription_element,int(price),fimage_element,fUrl_element,str(adresse_element.a.text)]
    result.append(infoData)

 with open('domians-immo.csv', 'a', encoding='utf8', newline='\n') as file :
     thewriter = writer(file)
     for item in result:
      thewriter.writerow(item)
#convertire les données CSV en JSON
data = []
with open('domians-immo.csv',encoding='utf8') as fcsv :
  csvReader = csv.DictReader(fcsv) 
  for rows in csvReader : 
       data.append(rows)
  for row in data :
     print ('bienPieces :',row['bienPieces'],'\n')
     row['bienPieces']=int(row['bienPieces'])
     row['prix']=int(row['prix'].replace(" ",""))
     row['images']=row['images'].split()
     if (row['bienType'].__contains__('Appartement')):
        row['bienType']='appartement'
     elif (row['bienType'].__contains__('Maison')):
          row['bienType']='maison'
     if (row['bienType'].__contains__('Terrain')):
        row['bienType']='terrain'
     elif (row['bienType'].__contains__('Local D')or row['bienType'].__contains__('Local C')or row['bienType'].__contains__('Locaux')):
          row['bienType']='localcommercial'
     if (row['bienType'].__contains__('Immeuble')):
        row['bienType']='immeuble'
     elif (row['bienType'].__contains__('Boutique')):
          row['bienType']='boutique'
     if (row['bienType'].__contains__('Parking')):
        row['bienType']='parking'
     elif (row['bienType'].__contains__('Bureau')):
          row['bienType']='bureau'
     if (row['bienType'].__contains__('Ferme')):
        row['bienType']='ferme'
     elif (row['bienType'].__contains__('Chateau')):
          row['bienType']='chateau'
     if (row['bienType'].__contains__('Loft')):
        row['bienType']='loft'
     elif (row['bienType'].__contains__('chambre de hot')):
          row['bienType']='chambredhots'
with open('domians-immo.json','a',encoding='utf-8') as jsonFile :
    jsonFile.write(json.dumps(data,indent=4))

